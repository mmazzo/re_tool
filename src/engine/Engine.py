import tkinter as tk

from tkinter import ttk

from src.tabs.MortgageTab import MortgageTab
from src.tabs.ExportTab import ExportTab
from src.tabs.RentTab import RentTab


class Engine:
    def __init__(self):
        window = tk.Tk()
        window.title("Real Estate Tool")
        window.minsize(350, 300)

        self.tabs = ttk.Notebook(window)
        self.mortgageTab = MortgageTab(self)
        self.rentTab = RentTab(self)
        self.exportTab = ExportTab(self)

        self.tabs.pack(expand=1, fill="both")

        window.mainloop()

    def get_mortgage_monthly_total(self):
        return self.mortgageTab.total_monthly

    def get_txt_mortgage_data(self):
        return self.mortgageTab.get_txt_mortgage_data()

    def get_csv_mortgage_data(self):
        return self.mortgageTab.get_csv_mortgage_data()

    def get_txt_rental_data(self):
        return self.rentTab.get_txt_rental_data()

    def get_csv_rental_data(self):
        return self.rentTab.get_csv_rental_data()


if __name__ == "__main__":
    engine = Engine()
