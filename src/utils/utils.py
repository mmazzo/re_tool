import numbers
import tkinter as tk


def get_currency_format(value, currency=""):
    if isinstance(value, numbers.Number):
        return adjust_sign("{}{:.2f}".format(currency, float(value)))
    elif getattr(value, '__module__', None) == tk.__name__:
        return adjust_sign("{}{}".format(currency, "{:.2f}".format(float(value.get())) if value.get() else "0.00"))
    return "Error"


def adjust_sign(value):
    if "-" in value:
        return "-{}".format(value.replace("-", ""))
    return value
