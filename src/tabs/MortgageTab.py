import tkinter as tk

from tkinter import HORIZONTAL
from tkinter import messagebox
from tkinter import ttk

from src.utils.utils import get_currency_format


class MortgageTab:
    def __init__(self, engine):
        self.listed_amount_entry_var = tk.StringVar()
        self.down_payment_entry_var = tk.StringVar()
        self.down_payment_type_dollar_entry_var = tk.BooleanVar()
        self.interest_rate_field_entry_var = tk.StringVar()
        self.interest_rate_scale_entry_var = tk.DoubleVar()
        self.mortgage_period_entry_var = tk.IntVar()
        self.hoa_amount_entry_var = tk.StringVar()
        self.misc_fees_entry_var = tk.StringVar()
        self.property_tax_entry_var = tk.StringVar()
        self.down_payment_text_var = tk.StringVar(value="Down Payment (%) ")
        self.total_mortgage_cost_text_var = tk.StringVar()
        self.monthly_mortgage_text_var = tk.StringVar()
        self.total_monthly_text_var = tk.StringVar()
        self.total_monthly = 0
        self.misc_fees_monthly = 0
        self.property_tax_monthly = 0
        self.hoa_monthly = 0
        self.monthly_payments = 0
        self.total_mortgage_cost = 0
        self.down_payment_amount = 0
        self.principal_loan_amount = 0
        self.create_mortgage_tab(engine.tabs)

    def create_mortgage_tab(self, tabs):
        mortgage_payment_tab = ttk.Frame(tabs)
        tabs.add(mortgage_payment_tab, text="Mortgage")

        # Reset Button
        reset_fields_button = tk.Button(mortgage_payment_tab, command=self.reset_fields, text="Reset Fields", width=45)
        reset_fields_button.grid(row=0, column=0, columnspan=3)

        # Listed Amount
        listed_amount_label = tk.Label(mortgage_payment_tab, text="Listed Amount")
        listed_amount_label.grid(row=1, column=0)
        listed_amount_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.listed_amount_entry_var)
        listed_amount_field.grid(row=1, column=1)

        # Down Payment Amount
        down_payment_label = tk.Label(mortgage_payment_tab, textvariable=self.down_payment_text_var)
        down_payment_label.grid(row=2, column=0)
        down_payment_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.down_payment_entry_var)
        down_payment_field.grid(row=2, column=1)
        down_payment_dollar_amount_check_box = tk.Checkbutton(mortgage_payment_tab, text="Use Dollar Amount ",
                                                              variable=self.down_payment_type_dollar_entry_var,
                                                              command=self.update_down_payment_label)
        down_payment_dollar_amount_check_box.grid(row=2, column=2)

        # Interest Rate
        interest_rate_label = tk.Label(mortgage_payment_tab, text="Interest Rate (%)")
        interest_rate_label.grid(row=3, column=0)
        self.interest_rate_field_entry_var.set(3.50)
        self.interest_rate_field_entry_var.trace("w", lambda name, index, mode, interest_rate_field_var=self.interest_rate_field_entry_var: self.adjust_interest_rate_scale(self.interest_rate_field_entry_var.get()))
        interest_rate_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.interest_rate_field_entry_var)
        interest_rate_field.grid(row=3, column=1)
        self.interest_rate_scale_entry_var.set(3.50)
        interest_rate_scale = tk.Scale(mortgage_payment_tab, variable=self.interest_rate_scale_entry_var, orient=HORIZONTAL, from_=0,
                                       to=7,
                                       resolution=.01, showvalue=0, command=self.adjust_interest_rate_field)
        interest_rate_scale.grid(row=3, column=2)

        # Mortgage Period
        mortgage_period_label = tk.Label(mortgage_payment_tab, text="Mortgage Period (Years) ")
        mortgage_period_label.grid(row=4, column=0)
        options = (10, 15, 20, 25, 30, 35, 40)
        self.mortgage_period_entry_var.set(30)
        mortgage_period_menu = tk.OptionMenu(mortgage_payment_tab, self.mortgage_period_entry_var, *options)
        mortgage_period_menu.grid(row=4, column=1)

        # Additional Fees
        hoa_label = tk.Label(mortgage_payment_tab, text="HOA Fees ")
        hoa_label.grid(row=5, column=0)
        hoa_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.hoa_amount_entry_var)
        hoa_field.grid(row=5, column=1)
        property_tax_label = tk.Label(mortgage_payment_tab, text="Property Taxes ")
        property_tax_label.grid(row=6, column=0)
        property_tax_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.property_tax_entry_var)
        property_tax_field.grid(row=6, column=1)
        misc_fees_label = tk.Label(mortgage_payment_tab, text="Misc. Fees ")
        misc_fees_label.grid(row=7, column=0)
        misc_fees_field = tk.Entry(mortgage_payment_tab, width=15, textvariable=self.misc_fees_entry_var)
        misc_fees_field.grid(row=7, column=1)

        # Calculate
        calculate_cost_button = tk.Button(mortgage_payment_tab, text="Calculate", command=self.calculate, width=45)
        calculate_cost_button.grid(row=8, column=0, columnspan=3)

        # Result Labels
        total_mortgage_cost_label = tk.Label(mortgage_payment_tab, width=45, textvariable=self.total_mortgage_cost_text_var)
        total_mortgage_cost_label.grid(row=9, column=0, columnspan=3)
        monthly_mortgage_label = tk.Label(mortgage_payment_tab, width=45, textvariable=self.monthly_mortgage_text_var)
        monthly_mortgage_label.grid(row=10, column=0, columnspan=3)
        total_monthly_label = tk.Label(mortgage_payment_tab, width=45, textvariable=self.total_monthly_text_var)
        total_monthly_label.grid(row=11, column=0, columnspan=3)

    # Reset all fields on tab to initial state
    def reset_fields(self):
        for field in self.__dict__:
            if isinstance(getattr(self, field), tk.StringVar().__class__):
                getattr(self, field).set("")
        if self.down_payment_type_dollar_entry_var:
            self.down_payment_type_dollar_entry_var.set(False)
        self.update_down_payment_label()
        self.interest_rate_scale_entry_var.set(3.5)
        self.adjust_interest_rate_field(self.interest_rate_scale_entry_var.get())
        self.mortgage_period_entry_var.set(30)

    # Change down payment input to dollar/percent
    def update_down_payment_label(self):
        self.down_payment_text_var.set("Down Payment (" + ("$" if self.down_payment_type_dollar_entry_var.get() else "%") + ")")

    # Set entry field to scale value
    def adjust_interest_rate_field(self, value):
        self.interest_rate_field_entry_var.set(value)

    # Adjust scale to match entry input if valid, otherwise change text to match input scale
    def adjust_interest_rate_scale(self, value):
        try:
            self.interest_rate_scale_entry_var.set(float(value))
        except ValueError:
            self.adjust_interest_rate_field(self.interest_rate_scale_entry_var.get())

    # Calculate monthly payments using input data
    def calculate(self):
        if self.down_payment_entry_var.get().isnumeric() & self.listed_amount_entry_var.get().isnumeric():
            self.down_payment_amount = float(self.down_payment_entry_var.get()) if self.down_payment_type_dollar_entry_var.get() else (
                        (float(self.down_payment_entry_var.get()) / 100.0) * float(self.listed_amount_entry_var.get()))
            self.principal_loan_amount = float(self.listed_amount_entry_var.get()) - self.down_payment_amount
        else:
            error_message = "Invalid entry for Listed Amount and/or Down Payment"
            messagebox.showerror("Invalid Input(s)", error_message)

        interest_rate = float(self.interest_rate_scale_entry_var.get()) / 1200
        number_of_months = int(self.mortgage_period_entry_var.get()) * 12

        self.monthly_payments = (self.principal_loan_amount * (interest_rate * ((1 + interest_rate) ** number_of_months))) / (
                    ((1 + interest_rate) ** number_of_months) - 1)

        self.total_mortgage_cost = float(self.monthly_payments) * number_of_months
        self.hoa_monthly = float(self.hoa_amount_entry_var.get()) if self.hoa_amount_entry_var.get().isnumeric() else 0
        self.property_tax_monthly = float(self.property_tax_entry_var.get()) if self.property_tax_entry_var.get().isnumeric() else 0
        self.misc_fees_monthly = float(self.misc_fees_entry_var.get()) if self.misc_fees_entry_var.get().isnumeric() else 0
        self.total_monthly = self.monthly_payments + self.hoa_monthly + self.property_tax_monthly + self.misc_fees_monthly

        self.total_mortgage_cost_text_var.set("Mortgage Total: {}".format(get_currency_format(self.total_mortgage_cost, "$")))
        self.monthly_mortgage_text_var.set("Monthly Mortgage: {}".format(get_currency_format(self.monthly_payments, "$")))
        self.total_monthly_text_var.set("Monthly Total: {}".format(get_currency_format(self.total_monthly, "$")))

    # Collect and send data for txt export
    def get_txt_mortgage_data(self):
        export_data = "Mortgage Details\n"
        export_data += "\tListed amount: {}\n".format(get_currency_format(self.listed_amount_entry_var, "$"))
        export_data += "\tDown Payment Amount: {}\n".format(get_currency_format(self.down_payment_amount, "$"))
        export_data += "\tLoan Amount: {}\n".format(get_currency_format(self.principal_loan_amount, "$"))
        export_data += "\tInterest Rate: {}\n".format(get_currency_format(self.interest_rate_scale_entry_var, "$"))
        export_data += "\tMortgage Period: {} years / {} months\n".format(self.mortgage_period_entry_var.get(),
                                                                          int(self.mortgage_period_entry_var.get())*12)
        export_data += "\tFees and Taxes\n"
        export_data += "\t\tHome Owners Association: {}\n".format(get_currency_format(self.hoa_monthly, "$"))
        export_data += "\t\tProperty Taxes: {}\n".format(get_currency_format(self.property_tax_monthly, "$"))
        export_data += "\t\tMiscellaneous: {}\n".format(get_currency_format(self.misc_fees_monthly, "$"))
        export_data += "\tPayments\n"
        export_data += "\t\tTotal mortgage cost: {}\n".format(get_currency_format(self.total_mortgage_cost, "$"))
        export_data += "\t\tMonthly mortgage payments: {}\n".format(get_currency_format(self.monthly_payments, "$"))
        export_data += "\t\tTotal monthly payments: {}\n".format(get_currency_format(self.total_monthly, "$"))
        return export_data

    # Collect and send data for csv export
    def get_csv_mortgage_data(self):
        export_data = [get_currency_format(self.listed_amount_entry_var, ""), get_currency_format(self.down_payment_amount,"")]
        if self.down_payment_amount > 0 and self.listed_amount_entry_var.get().isnumeric():
            down_payment_percent = (self.down_payment_amount / float(self.listed_amount_entry_var.get())) * 100
            export_data.append(get_currency_format(down_payment_percent, ""))
            loan_amount = float(self.listed_amount_entry_var.get()) - self.down_payment_amount
            export_data.append(get_currency_format(loan_amount, ""))
        else:
            export_data.append("0")
            export_data.append(get_currency_format(self.listed_amount_entry_var, ""))
        export_data.append(get_currency_format(self.interest_rate_scale_entry_var, ""))
        export_data.append(get_currency_format(self.mortgage_period_entry_var, ""))
        export_data.append(get_currency_format(self.hoa_monthly, ""))
        export_data.append(get_currency_format(self.property_tax_monthly, ""))
        export_data.append(get_currency_format(self.misc_fees_monthly, ""))
        export_data.append(get_currency_format(self.total_mortgage_cost, ""))
        export_data.append(get_currency_format(self.monthly_payments, ""))
        export_data.append(get_currency_format(self.total_monthly, ""))
        return export_data
