import tkinter as tk
import os
import csv
import datetime

from tkinter import ttk


class ExportTab:
    def __init__(self, engine):
        self.engine = engine
        self.house_address_entry_var = tk.StringVar()
        self.notes_entry_var = tk.StringVar()
        self.export_mortgage_data = tk.BooleanVar()
        self.export_rental_data = tk.BooleanVar()
        self.txt_export_radio_var = tk.IntVar()
        self.file_name_entry_var = tk.StringVar()
        self.create_export_tab(engine.tabs)

    # Build the export tab
    def create_export_tab(self, tabs):
        export_tab = ttk.Frame(tabs)
        tabs.add(export_tab, text="Export")

        # Reset Button
        reset_fields_button = tk.Button(export_tab, text="Reset Fields", width=45, command=self.reset_fields)
        reset_fields_button.grid(row=0, column=0, columnspan=3)

        # House Address
        house_address_label_frame = tk.LabelFrame(export_tab, text="Address")
        house_address_label_frame.grid(row=1, column=0, columnspan=3)
        house_address_field = tk.Entry(house_address_label_frame, width=30, textvariable=self.house_address_entry_var)
        house_address_field.grid(row=0, column=0)

        # Check boxes for other tabs
        checkbox_label_frame = tk.LabelFrame(export_tab, text="Export From")
        checkbox_label_frame.grid(row=2, column=0, columnspan=3)
        mortgage_check_box = tk.Checkbutton(checkbox_label_frame, variable=self.export_mortgage_data, text="Mortgage Data")
        mortgage_check_box.grid(row=0, column=0, sticky="W")
        rent_check_box = tk.Checkbutton(checkbox_label_frame, variable=self.export_rental_data, text="Rental Data")
        rent_check_box.grid(row=1, column=0, sticky="W")

        # Radio buttons for export type
        radio_button_label_frame = tk.LabelFrame(export_tab, text="Export To")
        radio_button_label_frame.grid(row=3, column=0, columnspan=3)
        txt_export_radio = tk.Radiobutton(radio_button_label_frame, variable=self.txt_export_radio_var, value=1, text="Text File")
        txt_export_radio.grid(row=0, column=0, columnspan=2)
        txt_export_radio.select()
        csv_export_radio = tk.Radiobutton(radio_button_label_frame, variable=self.txt_export_radio_var, value=2, text="CSV File")
        csv_export_radio.grid(row=1, column=0, columnspan=2)
        export_file_name_label = tk.Label(radio_button_label_frame, text="File Name")
        export_file_name_label.grid(row=2, column=0)
        export_file_name_field = tk.Entry(radio_button_label_frame, width=30, textvariable=self.file_name_entry_var)
        export_file_name_field.grid(row=2, column=1)

        # Notes
        notes_label_frame = tk.LabelFrame(export_tab, text="Notes")
        notes_label_frame.grid(row=4, column=0, columnspan=3)
        notes_entry = tk.Entry(notes_label_frame, width=45, textvariable=self.notes_entry_var)
        notes_entry.grid(row=0, column=0)

        # Export Button
        export_button = tk.Button(export_tab, text="Export", width=45, command=self.export)
        export_button.grid(row=5, column=0, columnspan=3)

    # Reset all fields on tab to initial state
    def reset_fields(self):
        for field in self.__dict__:
            if isinstance(getattr(self, field), tk.StringVar().__class__):
                getattr(self, field).set("")
        for field in self.__dict__:
            if isinstance(getattr(self, field), tk.BooleanVar().__class__):
                getattr(self, field).set(False)
        self.txt_export_radio_var.set(1)

    # Export property data to file
    def export(self):
        if self.txt_export_radio_var.get() == 1:
            self.text_export()
        else:
            self.csv_export()

    # Export to new or existing txt file
    def text_export(self):
        # Retrieve filename and generate if no name was entered
        filename = self.get_file_name(".txt")

        export_data = "Address: " + (self.house_address_entry_var.get() if (self.house_address_entry_var.get()) else "Unknown") + "\n"
        export_data += "Notes: " + self.notes_entry_var.get() + "\n"

        if self.export_mortgage_data.get():
            export_data += self.engine.get_txt_mortgage_data()
        if self.export_rental_data.get():
            export_data += self.engine.get_txt_rental_data()

        export_file = open(filename, "a+")
        export_file.write(export_data)
        export_file.close()

    # Export to new or existing csv file
    def csv_export(self):
        # Retrieve filename and generate if no name was entered
        filename = self.get_file_name(".csv")

        # CSV column headers
        header_row = ["Address", "Notes", "Listed Amount", "Down Payment($)", "Down Payment(%)", "Loan Amount",
                      "Interest Rate", "Mortgage Period", "HOA", "Property Taxes", "Other Fees", "Total Mortgage Cost",
                      "Monthly Mortgage Payments", "Total Monthly Payments", "Monthly Property Cost",
                      "Additional Monthly Cost", "Number of Units", "Total Property Cost", "Rental Income",
                      "Monthly Returns"]
        data_row = [self.house_address_entry_var.get(), self.notes_entry_var.get()]

        # Add mortgage data to array
        if self.export_mortgage_data.get():
            data_row.extend(self.engine.get_csv_mortgage_data())
        else:
            skip_mortgage_data_array = ["N/A"] * 12
            data_row.extend(skip_mortgage_data_array)

        # Add rental data to array
        if self.export_rental_data.get():
            data_row.extend(self.engine.get_csv_rental_data())
        else:
            skip_rental_data_array = ["N/A"] * 6
            data_row.extend(skip_rental_data_array)

        # Open and write to file, append if file exists, add header if file doesn't exist
        file_exists = os.path.exists(filename)
        with open(filename, 'a', newline='') as file:
            writer = csv.writer(file)
            if not file_exists:
                writer.writerow(header_row)
            writer.writerow(data_row)

    def get_file_name(self, extension):
        current_time = datetime.datetime.now()
        generated_filename = "export_{}{}".format(current_time.strftime("%m-%d-%Y"), extension)
        filename = self.file_name_entry_var.get() if self.file_name_entry_var.get() else generated_filename
        if not filename.endswith(extension):
            filename += extension
        return filename
