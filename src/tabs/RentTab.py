import tkinter as tk

from tkinter import ttk
from tkinter import messagebox

from src.utils.utils import get_currency_format


class RentTab:
    def __init__(self, engine):
        self.engine = engine
        self.unit_amounts_array = []
        self.total_monthly_payments_entry_var = tk.StringVar()
        self.other_expenses_entry_var = tk.StringVar()
        self.total_cost = 0
        self.rental_income = 0
        self.total_profit = 0
        self.total_cost_text_var = tk.StringVar()
        self.rental_income_text_var = tk.StringVar()
        self.total_profit_text_var = tk.StringVar()
        self.create_rent_tab(engine.tabs)

    def create_rent_tab(self, tabs):
        rent_tab = ttk.Frame(tabs)
        tabs.add(rent_tab, text="Rent")

        # Unit(s)
        units_frame = tk.LabelFrame(rent_tab, text="Units")
        units_frame.grid(row=3, column=0, columnspan=3)
        add_unit_button = tk.Button(units_frame, command=lambda: self.create_unit_row(units_frame), text="Add Unit",
                                    width=30)
        add_unit_button.grid(row=0, column=0, columnspan=3)
        self.create_unit_row(units_frame)

        # Reset Button
        reset_fields_button = tk.Button(rent_tab, command=lambda: self.reset_fields(units_frame), text="Reset Fields",
                                        width=45)
        reset_fields_button.grid(row=0, column=0, columnspan=3)

        # Total Monthly Payment
        total_monthly_payments_label = tk.Label(rent_tab, text="Total Monthly Payments")
        total_monthly_payments_label.grid(row=1, column=0)
        total_monthly_payment_field = tk.Entry(rent_tab, width=15, textvariable=self.total_monthly_payments_entry_var)
        total_monthly_payment_field.grid(row=1, column=1)
        pull_monthly_total_button = tk.Button(rent_tab, command=self.get_mortgage_monthly, text="Pull Monthly Total")
        pull_monthly_total_button.grid(row=1, column=2)

        # Other Expenses
        other_expenses_label = tk.Label(rent_tab, text="Expenses")
        other_expenses_label.grid(row=2, column=0)
        other_expenses_field = tk.Entry(rent_tab, width=15, textvariable=self.other_expenses_entry_var)
        other_expenses_field.grid(row=2, column=1)

        # Calculate
        calculate_button = tk.Button(rent_tab, command=self.calculate, text="Calculate", width=45)
        calculate_button.grid(row=4, column=0, columnspan=3)

        # Result Labels
        total_cost_label = tk.Label(rent_tab, width=45, textvariable=self.total_cost_text_var)
        total_cost_label.grid(row=5, column=0, columnspan=3)
        rental_income_label = tk.Label(rent_tab, width=45, textvariable=self.rental_income_text_var)
        rental_income_label.grid(row=6, column=0, columnspan=3)
        total_profit_label = tk.Label(rent_tab, width=45, textvariable=self.total_profit_text_var)
        total_profit_label.grid(row=7, column=0, columnspan=3)

    # Create a unit row and add it to the frame
    def create_unit_row(self, frame):
        number_of_units = len(self.unit_amounts_array)
        # Nested method to remove a selected row
        def remove_row():
            for index, var in enumerate(self.unit_amounts_array[number_of_units + 1:len(self.unit_amounts_array)],
                                        start=number_of_units):
                self.unit_amounts_array[index].set(self.unit_amounts_array[index + 1].get())
            grid_items = list(frame.grid_slaves(row=len(self.unit_amounts_array)))
            for item in grid_items:
                item.grid_forget()
            del self.unit_amounts_array[-1]

        unit_label = tk.Label(frame, width=10)
        unit_label.grid(row=number_of_units + 1, column=0)
        entry_var = tk.StringVar()
        unit_field = tk.Entry(frame, textvariable=entry_var, width=15)
        unit_field.grid(row=number_of_units + 1, column=1)
        remove_unit_button = tk.Button(frame, command=remove_row, text="-")
        remove_unit_button.grid(row=number_of_units + 1, column=2)
        unit_label.config(text="Unit {}".format(number_of_units + 1))
        self.unit_amounts_array.append(entry_var)

    # Reset all fields on tab to initial state
    def reset_fields(self, frame):
        for field in self.__dict__:
            if isinstance(getattr(self, field), tk.StringVar().__class__):
                getattr(self, field).set("")
        for index, unit in enumerate(self.unit_amounts_array[:len(self.unit_amounts_array)],
                                     start=1):
            grid_items = list(frame.grid_slaves(row=index))
            for item in grid_items:
                item.grid_forget()
        self.unit_amounts_array = []
        self.create_unit_row(frame)
        self.total_cost = 0
        self.rental_income = 0
        self.total_profit = 0

    # Fill monthly payments field with value from mortgage tab
    def get_mortgage_monthly(self):
        self.total_monthly_payments_entry_var.set(get_currency_format(self.engine.get_mortgage_monthly_total()))

    # Collect and return data for csv export
    def calculate(self):
        self.total_cost = 0
        self.rental_income = 0
        self.total_profit = 0

        try:
            self.total_cost += float(self.total_monthly_payments_entry_var.get())
        except ValueError:
            error_message = "Unable To Calculate - Monthly Payments Invalid"
            messagebox.showerror("Invalid Monthly Payments", error_message)
            return

        if self.other_expenses_entry_var.get() and not self.other_expenses_entry_var.get().isnumeric():
            error_message = "Unable To Calculate - Other Expenses Invalid"
            messagebox.showerror("Invalid Amount", error_message)
            return
        self.total_cost += float(self.other_expenses_entry_var.get())

        for index, unit in enumerate(self.unit_amounts_array):
            if unit.get():
                if unit.get().isnumeric():
                    self.rental_income += float(unit.get())
                else:
                    error_message = "Unit {} contains an invalid value {}. Skip this unit and continue calculation?".format(index, unit.get())
                    if not messagebox.askyesno("Invalid Amount", error_message, icon="warning"):
                        return

        self.total_profit = self.rental_income - self.total_cost
        self.total_cost_text_var.set("Monthly Cost: {}".format(get_currency_format(self.total_cost, "$")))
        self.rental_income_text_var.set("Rental Income: {}".format(get_currency_format(self.rental_income, "$")))
        self.total_profit_text_var.set("Profit: {}".format(get_currency_format(self.total_profit, "$")))

    # Collect and return data for txt export
    def get_txt_rental_data(self):
        export_data = "Rental Details\n"
        export_data += "\tMonthly Property Cost: {}\n".format(get_currency_format(self.total_monthly_payments_entry_var,"$"))
        export_data += "\tAdditional Monthly Cost: {}\n".format(get_currency_format(self.other_expenses_entry_var, "$"))
        export_data += "\tRental Units\n"
        for index, unit in enumerate(self.unit_amounts_array):
            export_data += "\t\tUnit {}: {}\n".format(index + 1, get_currency_format(unit, "$"))
        export_data += "\tMonthly Amounts\n"
        export_data += "\t\tProperty Cost: {}\n".format(get_currency_format(self.total_cost, "$"))
        export_data += "\t\tRental Income: {}\n".format(get_currency_format(self.rental_income, "$"))
        export_data += "\t\tProfit: {}\n".format(get_currency_format(self.total_profit, "$"))
        return export_data

    # Collect and send data for csv export
    def get_csv_rental_data(self):
        return [get_currency_format(self.total_monthly_payments_entry_var, ""),
                get_currency_format(self.other_expenses_entry_var, ""), str(len(self.unit_amounts_array)),
                get_currency_format(self.total_cost), get_currency_format(self.rental_income),
                get_currency_format(self.total_profit)]
