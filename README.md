# Real Estate Tool
##### Author: Mike Mazzola
##### Version: 1.0

#### The Real Estate Tool is a Python application that allows the user to calculate mortgage/monthly payments for a property as well as rental income/profit. It uses the Tkinter library to create a GUI containing three separate tabs: Mortgage, Rent, and Export. The user can export the data from each tab into a new or existing .txt or .csv file. Each tab has a user friendly design for collecting information as well as a button to reset to the initial state.
## Engine
The Engine class is the core of the application. It initializes the window and creates the Mortgage, Rent, and Export tab. Each tab has a reference to the Engine instance and can use this to pull data from other tabs as well as add widgets to the main window.
## Tabs
### Mortgage Tab
The Mortgage Tab is used to calculate the mortgage information of a property. It is the initial tab displayed once the application is started. For input, it takes the listed amount, down payment amount (supports dollar amount and percentage), interest rate percent, mortgage period, and additional optional fees: Home Owner Association, Property Taxes, and Miscellaneous. When the calculate button is clicked it will display the total amount to be paid for the mortgage, the monthly mortgage amount, and the monthly total including the additional fees. 
![Mortgage Tab](/screenshots/Mortgage_Tab_Initial.png)
![Mortgage Tab](/screenshots/Mortgage_Tab_Filled.png)

### Rent Tab
The Rent Tab is used to calculate the profit potential of a rental property. It takes in all monthly costs and any additional expenses of the property as well as each individual unit and the amount they will rent for. Units can be added and removed dynamically by clicking the designated buttons. The tab also allows for the total monthly payment amount to be imported from the Mortgage Tab (if the mortgage tab is not complete the value will be set to 0.00). When the calculate button is clicked it will display the total monthly cost of the property, the rental income, and the profit gained, which can be positive or negative.
![Rent Tab](/screenshots/Rent_Tab_Initial.png)
![Rent Tab](/screenshots/Rent_Tab_Filled.png)

### Export Tab
The Export Tab is used to export the data collected from the mortgage and/or rent tab into a new or existing file. Exporting supports both ".txt" and ".csv" files. This tab allows the user to enter the address of the property, choose which tabs to export data from, select the type of file export, and enter the name of the new or existing file. Additionally, a note can be added before exporting the file. 
![Export Tab](/screenshots/Export_Tab_Initial.png)
![Export Tab](/screenshots/Export_Tab_Filled.png)
![CSV Export](/screenshots/CSV_File_Example.png)
![Text Export](/screenshots/Text_File_Example.png)
